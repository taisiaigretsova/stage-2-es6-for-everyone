import { showModal } from "./modal";
import { createFighterImage } from "../fighterPreview";
import App from "../../app";

export function showWinnerModal(fighter) {
  // call showModal function 
  console.log(fighter) 
  return showModal({title: `${fighter.name} win`, bodyElement: createFighterImage(fighter), onClose: onModalClose});
}


function onModalClose() {
  document.querySelector('.arena___root').remove();
  new App();
}