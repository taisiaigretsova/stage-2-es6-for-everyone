import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    // Бійці можуть блокувати удари за допомогою клавіш D та L відповідно, 
    //у такому випадку боєць ухиляється від удару. Також боєць не може завдати удар, якщо він знаходиться у блоці.
    
    const firstPlayerAttack = controls.PlayerOneAttack;
    const secondPlayerAttack = controls.PlayerTwoAttack;
    const firstPlayerBlock = controls.PlayerOneBlock;
    const secondPlayerBlock = controls.PlayerTwoBlock;
    
    let firstFighterHealth = firstFighter.health;
    let secondFighterHealth = secondFighter.health;
    let isFirstPlayerBlock = false;
    let isSecondPlayerBlock = false;


    document.onkeydown = (e) => {
      const keyName = e.code;

      if(keyName === firstPlayerBlock) {
        isFirstPlayerBlock = true;
      }
      if(keyName === secondPlayerBlock) {
        isSecondPlayerBlock = true;
      }
      
      if (keyName === firstPlayerAttack && !isFirstPlayerBlock && !isSecondPlayerBlock) {
        secondFighterHealth -= getDamage(firstFighter, secondFighter, isSecondPlayerBlock);
        console.log(secondFighterHealth)
        document.getElementById('right-fighter-indicator').style = `width: ${secondFighterHealth / secondFighter.health * 100}%`
      }
      
      if (keyName === secondPlayerAttack && !isSecondPlayerBlock && !isFirstPlayerBlock) {
        firstFighterHealth -= getDamage(secondFighter, firstFighter, isFirstPlayerBlock);
        
        document.getElementById('left-fighter-indicator').style = `width: ${firstFighterHealth / firstFighter.health * 100}%`
      }
      if (secondFighterHealth <= 0) {
        resolve(firstFighter)
      } else  if (firstFighterHealth <= 0) {
        resolve(secondFighter);
      } 
    }
    document.onkeyup = (e) => {
      const keyName = e.code;
      if(keyName === firstPlayerBlock) {
        isFirstPlayerBlock = false;
      }
      if(keyName === secondPlayerBlock) {
        isSecondPlayerBlock = false;
      }
    }
  });

}

export function getDamage(attacker, defender) {
  // return damage
// getHitPower - getBlockPower (або ж 0, якщо боєць "ухилився" від удару повністю, тобто сила блоку більша за силу удару).
  const blockPower = getBlockPower(defender);
  let damage = getHitPower(attacker) - blockPower;
  if (damage < 0) {
    damage = 0;
  }
  console.log(damage)
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  // power = attack * criticalHitChance;, де criticalHitChance — рандомне число від 1 до 2
  const criticalHitChance = getRandomChance();
  let hitPower = fighter.attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  // return block power = defense * dodgeChance;, де dodgeChance — рандомне число від 1 до 2.
  const dodgeChance = getRandomChance();
  let blockPower = fighter.defense * dodgeChance;
  return blockPower;
}


function getRandomChance () {
  return Math.random() + 1; 
}
