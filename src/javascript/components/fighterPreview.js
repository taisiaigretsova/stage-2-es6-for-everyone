import { createElement } from '../helpers/domHelper';
import { getFighterDetails } from './fighterSelector';

export function createFighterPreview(fighter, position) {
    const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
    const fighterElement = createElement({
        tagName: 'div',
        className: `fighter-preview___root ${positionClassName}`,
    });

    // todo: show fighter info (image, name, health, etc.)

    if (fighter) {

        const fighterImageEl = createFighterImage(fighter);
        const fighterInfoEl = createFighterInfo(fighter);

        fighterElement.append(fighterImageEl);
        fighterElement.append(fighterInfoEl);

    }


    return fighterElement;
}

export function createFighterImage(fighter) {
    const { source, name } = fighter;
    const attributes = {
        src: source,
        title: name,
        alt: name
    };
    const imgElement = createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes,
    });

    return imgElement;
}

export function createFighterInfo(fighter) {
    const { name } = fighter;
    const nameElement = createElement({
        tagName: 'h3',
        className: 'fighter-preview___info',
    });
    nameElement.innerText = `${name} (health: ${fighter.health} attack: ${fighter.attack})`;

    return nameElement;
}